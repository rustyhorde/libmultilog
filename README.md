# libmultilog
Logging implementations (stdout, file, socket, and various dbs) in Rust

## Status
[![Crates IO](https://img.shields.io/crates/v/libmultilog.svg)](https://crates.io/crates/libmultilog)
[![Build Status](https://travis-ci.org/rustyhorde/libmultilog.svg?branch=master)](https://travis-ci.org/rustyhorde/libmultilog)
[![Coverage Status](https://coveralls.io/repos/rustyhorde/libmultilog/badge.svg?branch=master&service=github)](https://coveralls.io/github/rustyhorde/libmultilog?branch=master)

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
