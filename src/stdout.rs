//! stdout Logger.
use log::{self, LogRecord, LogLevelFilter, LogMetadata, SetLoggerError};
use std::sync::{Arc, Mutex};

/// StdoutLogger.
pub struct StdoutLogger {
    outputfn: Arc<Mutex<fn(&LogRecord)>>,
}

impl StdoutLogger {
    /// Create a new StdoutLogger.
    pub fn new(outputfn: fn(&LogRecord)) -> StdoutLogger {
        StdoutLogger { outputfn: Arc::new(Mutex::new(outputfn)) }
    }
}

impl log::Log for StdoutLogger {
    fn enabled(&self, _metadata: &LogMetadata) -> bool {
        true
    }

    fn log(&self, record: &LogRecord) {
        if self.enabled(record.metadata()) {
            match self.outputfn.lock() {
                Ok(ref mut f) => f(record),
                Err(_) => {}
            }
        }
    }
}

/// Initialize the stdout logger.
pub fn init_stdout_logger(level: LogLevelFilter,
                          logger: StdoutLogger)
                          -> Result<(), SetLoggerError> {
    log::set_logger(|max_log_level| {
        max_log_level.set(level);
        Box::new(logger)
    })
}
