// Copyright (c) 2016 vergen developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! libmultilog
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
#[macro_use]
extern crate blastfig;
#[macro_use]
extern crate log;
#[cfg(feature = "mysql")]
extern crate mysql;
extern crate regex;
#[cfg(feature = "rusqlite")]
extern crate rusqlite;
extern crate time;

pub mod multi;
pub mod file;
#[cfg(feature = "mysql")]
pub mod mysqll;
#[cfg(feature = "socket")]
pub mod socket;
#[cfg(feature = "rusqlite")]
pub mod sqlite;
pub mod stdout;
mod version;

pub use version::version;

#[cfg(test)]
mod test {
    use super::version;

    #[test]
    fn test_version() {
        assert!(!version::version(true, true).is_empty());
    }
}
