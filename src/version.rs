include!(concat!(env!("OUT_DIR"), "/version.rs"));

/// Generate version information.
pub fn version(verbose: bool, colorize: bool) -> String {
    match (verbose, colorize) {
        (true, true) => {
            format!("\x1b[32;1mml {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
                     {}\nbuild-date: {}\nhost: {}\nrelease: {}\n",
                    semver(),
                    short_sha(),
                    commit_date(),
                    short_now(),
                    sha(),
                    commit_date(),
                    short_now(),
                    target(),
                    semver())
        }
        (true, false) => {
            format!("ml {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: \
                     {}\nhost: {}\nrelease: {}\n",
                    semver(),
                    short_sha(),
                    commit_date(),
                    short_now(),
                    sha(),
                    commit_date(),
                    short_now(),
                    target(),
                    semver())
        }
        (false, true) => {
            format!("\x1b[32;1mml {}\x1b[0m ({} {}) (built {})\n",
                    semver(),
                    short_sha(),
                    commit_date(),
                    short_now())
        }
        (false, false) => {
            format!("rh {} ({} {}) (built {})\n",
                    semver(),
                    short_sha(),
                    commit_date(),
                    short_now())
        }
    }
}
